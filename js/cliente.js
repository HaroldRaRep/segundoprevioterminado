document.addEventListener('DOMContentLoaded', function () {
    const clienteInfoDiv = document.getElementById('clienteInfo');

    // Obtener valores del Local Storage
    const email = localStorage.getItem('email');
    const fechaNacimiento = localStorage.getItem('fechaNacimiento');
    const sexo = localStorage.getItem('sexo');

    // Obtener objeto de persona seleccionada del JSON personas.json
    const persona = JSON.parse(localStorage.getItem('personaSeleccionada'));

   // Crear elementos para mostrar la información
const rowDiv = document.createElement('div');
rowDiv.classList.add('row');

const imageDiv = document.createElement('div');
imageDiv.classList.add('col-md-4', 'mb-4', 'text-center');

const infoDiv = document.createElement('div');
infoDiv.classList.add('col-md-8');

const imagen = document.createElement('img');
imagen.src = (sexo === 'Mujer') ? 'assets/mujer.png' : 'assets/hombre.png';
imagen.classList.add('img-fluid');

const nombreElemento = document.createElement('p');
nombreElemento.textContent = `Nombre: ${persona.Nombre} ${persona.Apellidos}`;

const emailElemento = document.createElement('p');
emailElemento.textContent = 'Email: ' + email;

const fechaNacimientoElemento = document.createElement('p');
fechaNacimientoElemento.textContent = 'Fecha de Nacimiento: ' + fechaNacimiento;

const numeroElemento = document.createElement('p');
numeroElemento.textContent = `Número: ${persona.Celular}`;

// Añadir elementos a los contenedores correspondientes
imageDiv.appendChild(imagen);

infoDiv.appendChild(nombreElemento);
infoDiv.appendChild(emailElemento);
infoDiv.appendChild(fechaNacimientoElemento);
infoDiv.appendChild(numeroElemento);

rowDiv.appendChild(imageDiv);
rowDiv.appendChild(infoDiv);

// Agregar el contenido al clienteInfoDiv
clienteInfoDiv.appendChild(rowDiv);

});
